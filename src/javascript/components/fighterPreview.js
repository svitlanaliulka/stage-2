import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

 function createFighterInfo(fighter){
    const img = createFighterImage(fighter);
    const {name, health, attack, defense } = fighter;
    const abilities = {
      health: health,
      attack: attack,
      defense: defense
    }
      const fighterAbilities = createElement({ 
        tagName: 'div', 
        className: `fighter-preview___abilities`,
        abilities,
      });
      fighterAbilities.setAttribute('style', 'white-space: pre;');
      fighterAbilities.textContent = `${name} \n health - ${health}\n attack - ${attack}\n defense - ${defense}`;
      fighterElement.append(img, fighterAbilities);
  }
  createFighterInfo(fighter);
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };

  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
