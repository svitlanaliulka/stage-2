  import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  const title = `Winner is ${fighter.name}`;
 
  showModal({title});
}

